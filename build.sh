#!/bin/bash

set -eo pipefail

VERSION=${VERSION:-"0.17.0"}
RELEASE=${RELEASE:-"1"}
URL="https://github.com/prometheus/node_exporter/releases/download/v${VERSION}/node_exporter-${VERSION}.linux-amd64.tar.gz"

build() {
  pushd rootfs/
    fpm -n node_exporter \
    -m "williecadete@gmail.com" \
    --rpm-summary "Exporter for machine metrics" \
    --description "Prometheus exporter for hardware and OS metrics exposed by *NIX kernels, written in Go with pluggable metric collectors." \
    --url "https://prometheus.io/" \
    --license "Apache License 2.0" \
    -s dir \
    -t rpm \
    --iteration $RELEASE\
    -v $VERSION \
    -p ../ \
    --prefix / \
    etc usr
  popd
}

get_tarball(){
  test -d ./rootfs/usr/sbin || mkdir ./rootfs/usr/sbin
  test -f /tmp/node_exporter*.gz || wget -q $URL -P /tmp/
  tar -xf /tmp/node_exporter-${VERSION}.linux-amd64.tar.gz -C ./rootfs/usr/sbin/ node_exporter-${VERSION}.linux-amd64/node_exporter --strip-components 1
}

get_tarball && build